const UserModel = require("./model");
class UserController {
  getAllUsers = async (req, res) => {
    const allUser = await UserModel.getAllUser();
    return res.json(allUser);
  };
  getSingleUser = async (req, res) => {
    const { idUser } = req.params;
    try {
      const users = await UserModel.getSingleUser(idUser);
      if (users) {
        return res.json(users);
      } else {
        res.statusCode = 400;
        return res.json({ message: "User id tidak ditemukan : " + idUser });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "User id tidak ditemukan : " + idUser });
    }
  };

  getsingleuserbio = async (req, res) => {
    const { idUser } = req.params;
    const findbio = await UserModel.getsingleuserbio(idUser);
    try {
      if (findbio) {
        return res.json(findbio);
      } else {
        res.statusCode = 400;
        return res.json({
          message: "biodata user tidak ditemukan : " + idUser,
        });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "biodata user tidak ditemukan : " + idUser });
    }
  };
  updateUserBio = async (req, res) => {
    const { idUser } = req.params;
    const { fullname, PhoneNumber, Address } = req.body;

    const updateuser = await UserModel.updateUserBio(
      idUser,
      fullname,
      PhoneNumber,
      Address
    );

    return res.json(updateuser);
  };

  // updateGamehistory = async (req, res) => {
  //   const { idUser } = req.params;
  //   const { status } = req.body;

  //   const updateHistory = await UserModel.updateGameHistory(idUser, status);

  //   return res.json(updateHistory);
  // };

  registerUsers = async (req, res) => {
    const dataRequest = req.body;

    //  cek apakah username, email dan password benar
    if (dataRequest.username === undefined || dataRequest.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username is invalid" });
    }

    if (dataRequest.email === undefined || dataRequest.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email is invalid" });
    }

    if (dataRequest.password === undefined || dataRequest.password === "") {
      res.statusCode = 400;
      return res.json({ message: "Password is invalid" });
    }
    // if (dataRequest.fullname === undefined || dataRequest.fullname === "") {
    //   res.statusCode = 400;
    //   return res.json({ message: "fullname is invalid" });
    // }

    //   cek apakah username dan email sudah teregistrasi
    const existData = await UserModel.isUserRegistered(dataRequest);
    // data.fullname === dataRequest.fullname

    if (existData) {
      return res.json({ message: "Username or email is exist!" });
    }

    //   record data kedalam daftarUser
    UserModel.recordNewdata(dataRequest);

    res.json({ message: "Sukses menambahkan User baru!!" });
  };

  registerBio = async (req, res) => {
    const dataBio = req.body;
    try {
      if (dataBio.fullname === undefined || dataBio.fullname === "") {
        res.statusCode = 400;
        return res.json({ message: "mohon diisi nama lengkap" });
      }

      if (dataBio.PhoneNumber === undefined || dataBio.PhoneNumber === "") {
        res.statusCode = 400;
        return res.json({ message: "mohon diisi nomor hp" });
      }

      if (dataBio.Address === undefined || dataBio.Address === "") {
        res.statusCode = 400;
        return res.json({ message: "mohon diisi alamat " });
      }
      if (dataBio.User_id === undefined || dataBio.User_id === "") {
        res.statusCode = 400;
        return res.json({
          message: " mohon diisi dengan id yang telah anda dapatkan ",
        });
      }
    } catch (error) {
      console.log(error);
    }

    //   cek apakah biodata ada yang sama
    const existBio = await UserModel.isUserBioRegistered(dataBio);

    if (existBio) {
      return res.json({ message: "biodata sudah ada" });
    }
    UserModel.recordNewdatabio(dataBio);

    res.json({ message: "Sukses menambahkan biodata baru!!" });
  };

  inputGamehistory = async (req, res) => {
    const dataHistory = req.body;
    try {
      if (dataHistory.User_id === undefined || dataBio.User_id === "") {
        res.statusCode = 400;
        return res.json({
          message: " mohon diisi dengan id yang telah anda dapatkan ",
        });
      }
      if (dataHistory.status === undefined || dataHistory.status === "") {
        res.statusCode = 400;
        return res.json({ message: "ayo main dulu" });
      }
    } catch (error) {
      console.log(error);
    }

    UserModel.recordNewdatahistory(dataHistory);
    res.json({ message: "User id sudah main" });
  };

  userLogin = async (req, res) => {
    const { username, password } = req.body;
    const dataLogin = await UserModel.verifyLogin(username, password);
    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      return res.json({ message: "credential tidak cocok" });
    }
  };
  getsingleuserhistory = async (req, res) => {
    const { idUser } = req.params;
    const userhistory = await UserModel.getsingleuserhistory(idUser);
    try {
      if (userhistory) {
        return res.json(userhistory);
      } else {
        res.statusCode = 400;
        return res.json({
          message: "history user tidak ditemukan : " + idUser,
        });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "History user tidak ditemukan : " + idUser });
    }
  };
}

module.exports = new UserController();
